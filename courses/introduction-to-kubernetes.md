# Introduction to Kubernetes

## From monolith to microservices

Refactoring types:
  1. Big Bang. Postpone features until everything is refactored. Bad.
  2. Incremental. Keep developing features while refactoring out of the monolith
     and communicating by an API. Good.
